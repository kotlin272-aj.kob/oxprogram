class Player(name:String) {
	val name = name
	var win = 0
	var lost = 0
	var draw = 0
	
	fun addWin(){
		win++
	}
	fun addLost(){
		lost++
	}
	fun addDraw(){
		draw++
	}
	
	fun showScore (){
		println("${name} have score : win ${win} | lost ${lost} | draw ${draw}")
	}
}
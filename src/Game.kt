import java.util.Scanner

class Game {
	var PlayerX: Player = Player("x")
	var PlayerO: Player = Player("o")

	var x: Int = -1
	var y: Int = -1

	var turn: Char = 'x'
	val board = Board()

	fun PlayGame() {
		showScore()
		board.displayBoard()
		displayTurn()
		while (true) {
			inputPosition()
			if (board.choosePosition(x, y, turn)) {
				break
			} else {
				println("Please don't choose position dupicate")
			}
		}
		if (board.checkWin()) {
			showWinner()
			addScoreWin()
			board.displayBoard()
			board.restartBoard()
		}
		if (board.checkDraw()) {
			showDraw()
			addScoreDraw()
			board.displayBoard()
			board.restartBoard()
		}
		switchTurn()
	}

	fun showWinner() {
		println("### Winner is ${turn} ###")
	}

	fun showDraw() {
		println("### This game is Draw ###")
		turn = 'x'
	}

	fun showScore() {
		println("----------------------------------------")
		PlayerX.showScore()
		PlayerO.showScore()
		println("----------------------------------------")
	}

	fun addScoreWin() {
		if (turn == 'x') {
			PlayerX.addWin()
			PlayerO.addLost()
		} else {
			PlayerO.addWin()
			PlayerX.addLost()
		}
	}

	fun addScoreDraw() {
		PlayerX.addDraw()
		PlayerO.addDraw()
	}

	fun inputPosition() {
		val scanner = Scanner(System.`in`)
		while (true) {
			try {
				print("Please input row column :")
				x = scanner.nextInt()
				y = scanner.nextInt()
				if (x < 3 && x >= 0 && y < 3 && y >= 0) {
					break
				} else {
					println("Please input row column between 0..2")
				}
			}catch( e: Exception) {
				scanner.nextLine()
				println("Please input row column between 0..2")
				continue
			}
		}
	}

	fun switchTurn() {
		turn = if (turn == 'x') 'o' else 'x'
	}

	fun displayTurn() {
		println("This turn is ${turn}")
	}
}

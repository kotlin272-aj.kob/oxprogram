class Board(){
	val board = arrayListOf(charArrayOf(' ',' ',' '),
							charArrayOf(' ',' ',' '),
							charArrayOf(' ',' ',' '))
	
	fun choosePosition(x:Int,y:Int,turn:Char) : Boolean{
		if(board[x][y] == ' '){
			board[x][y] = turn
			return true
		}else{
			return false
		}
	}
	
	fun displayBoard(){
		for(i in 0..2){
			for(j in 0..2){
				print("|${board[i][j]}")
			}
			println("|")
		}
	}
	
	fun restartBoard(){
		for(i in 0..2){
			for(j in 0..2){
				board[i][j] = ' '
			}
		}
	}
	
	fun checkDraw():Boolean{
		for(i in 0..2){
			for(j in 0..2){
				if (board[i][j] == ' '){
					return false
				}
			}
		}
		return true
	}
	
	fun checkWin() : Boolean{
		if(winRow()){
			return true
		}
		if(winCell()){
			return true
		}
		if(winCross()){
			return true
		}
		return false
	}
	
	fun winRow() : Boolean{
		for(i in 0..2){
			if(board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][0] == board[i][2]  &&  board[i][2] != ' '){
				return true
			}
		}
		return false
	}
	
	fun winCell() : Boolean{
		for(i in 0..2){
			if(board[0][i] == board[1][i] && board[1][i] == board[2][i] && board[0][i] == board[2][i] && board[2][i] != ' '){
				return true
			}
		}
		return false
	}
	
	fun winCross() : Boolean{
			if(board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[0][0] == board[2][2] && board[0][0] != ' '){
				return true
			}
			if(board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[0][2] == board[2][0] && board[0][2] != ' '){
				return true
			}
		return false
	}
}